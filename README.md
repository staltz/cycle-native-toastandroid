# Cycle Native ToastAndroid

**A Cycle.js Driver for interacting with React Native's [ToastAndroid](http://facebook.github.io/react-native/docs/toastandroid)**

```
npm install cycle-native-toastandroid
```

## Usage

### Sink

Stream of a toast object that can either be:

```js
{
  type: 'show',
  message: /* string */,
  duration: /* number */
}
```

or:

```js
{
  type: 'showWithGravity',
  message: /* string */,
  duration: /* number */,
  gravity: /* ToastAndroid.TOP or ToastAndroid.CENTER or ToastAndroid.BOTTOM */
}
```

### Source

No source.

## License

Copyright (C) 2018 Andre 'Staltz' Medeiros, licensed under MIT license
